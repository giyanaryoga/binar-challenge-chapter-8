import React from 'react';

function Create() {
    return(
        <div className="container mt-5 mb-5 justify-center">
            <h1>Tambah Player Baru:</h1>
                <form>
                    <label>Username: </label>
                    <input type="text" name="username" required/>
                    <br/><br/>
                    <label>Password: </label>
                    <input type="password" name="password"/>
                    <br/><br/>
                    <label>Email: </label>
                    <input type="text" name="email"/>
                    <br/><br/>
                    <label>Experience: </label>
                    <input type="text" name="experience" value="0"/>
                    <br/><br/>
                    <label>Level: </label>
                    <input type="text" name="level"/>
                    <br/><br/>
                    <button type="submit" className="btn btn-primary" name="Create">Submit</button>
                </form>
        </div>
    );
}

export default Create;